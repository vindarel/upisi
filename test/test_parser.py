#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, sys
import unittest

from textwrap import dedent
from pprint import pprint

common_dir = os.path.dirname(os.path.abspath(__file__))
cdp, _ = os.path.split(common_dir)
sys.path.append(cdp)
from scriptparser import parser, main
from scriptparser import txt_to_lines
from scriptparser import next_doc
from scriptparser import is_comment
from scriptparser import next_lines_no_comment

class TestParser(unittest.TestCase):

    def setUp(self):
        pass

    def test_parser(self):
        title = "a title"
        cmd = "a cmd\\"
        cmd2 = "on multiple\\"
        cmd3 = "lines"
        im = "/path/to/an/image"
        doc = "some doc"
        more_doc = "and more doc"
        toggle = "True"
        txt = dedent("""
        #: {}
        #+im: {}
        #+gui:toggle={}
        #+doc: {}
        # {}
        {}
        {}
        {}
        #: nop
        """).format(title, im, toggle, doc, more_doc,
                    cmd, cmd2, cmd3)
        items, _ = main(txt)
        self.assertTrue(items)
        self.assertEqual(items[0].get("title"), title)
        self.assertTrue(cmd in items[0].get("cmd"))
        self.assertEqual(items[0].get("im"), im)
        self.assertEqual(items[0].get("doc"), doc + " " + more_doc)
        self.assertEqual(items[0].get("gui_toggle"), toggle)
        self.assertTrue(more_doc in items[0].get("doc"), "doc on multiple lines")
        self.assertTrue(cmd3 in items[0].get("cmd"), "command on multiple lines.")

    def test_doc_multilines(self):
        txt = dedent("""
        #+doc: one
        # two
        # three
        #+foo: bar
        """)
        lines = txt_to_lines(txt)
        doc = next_doc(0, lines)
        self.assertTrue("one" in doc)
        self.assertTrue("three" in doc)
        self.assertTrue("bar" not in doc)

    def test_doc_multilines_and_cmd(self):
        txt = dedent("""
        #+doc: one
        # two
        nop
        """)
        lines = txt_to_lines(txt)
        doc = next_doc(0, lines)
        self.assertTrue("one" in doc)
        self.assertTrue("nop" not in doc)

    def test_get_commands(self):
        txt = dedent("""
        # count from just after the title
        # ---
        one
        two
        #: nop
        """)
        cmd = next_lines_no_comment(0, txt_to_lines(txt))
        self.assertTrue("one" in cmd)
        self.assertTrue("two" in cmd)
        self.assertTrue("nop" not in cmd)

    def test_get_commands_no_command(self):
        txt = dedent("""
        #+doc: doc
        # comment
        #: next item
        next cmd
        """)
        cmd = next_lines_no_comment(0, txt_to_lines(txt))
        self.assertFalse(cmd)

    def test_txt_to_lines(self):
        txt = dedent("""
        #: void
        #: one
        """)
        self.assertEqual(txt_to_lines(txt)[0], "#: one")

    def test_many_items(self):
        """Check that we always detect the right number of items.
        """
        txt = dedent("""
        #: one
        #
        one cmd
        #
        #: void
        #: two
        #
        #: three
        """)
        items = main(txt)
        self.assertEqual(len(items), 3)

    def test_is_comment(self):
        line = "#: a marker comment"
        self.assertTrue(is_comment(line))
        self.assertFalse(is_comment(line, ignore_marker=False))

    def test_categories(self):
        """Recursive parsing inside categories."""

        txt = dedent("""
        #: one
        one cmd
        #+cat: first cat
        #+doc: cat's doc
        #: cat's cmd title
        cat's cmd
        #: second cmd
        do cat's 2nd cmd
        #+end_cat
        #: two, outside cat.
        two's cmd
        """)
        items, _ = main(txt)
        self.assertTrue(items[1].get("cat"))
        self.assertTrue(len(items), 3)
        self.assertEqual(items[1]["items"][0]["cmd"], "cat's cmd")

        tohave = [{'doc': None,
                   'gui_toggle': None,
                   'cmd': 'one cmd', 'im': None,
                   'title': 'one'},
                  {'doc': "cat's doc",
                   'items': [{'doc': None,
                              'gui_toggle': None,
                              'cmd': "cat's cmd",
                              'im': None,
                              'title': "cat's cmd title"},
                             {'doc': None,
                              'gui_toggle': None,
                              'cmd': "do cat's 2nd cmd",
                              'im': None,
                              'title': 'second cmd'}],
                   'cat': 'first cat'},
                  {'doc': None,
                   'gui_toggle': None,
                   'cmd': "two's cmd",
                   'im': None,
                   'title': 'two, outside cat.'}]

        self.assertEqual(items, tohave)


#     def test_categories_many(self):
#         txt = dedent("""
#         #: one
#         one cmd
#         #+cat: first cat
#         #+doc: cat's doc
#         #: cat's cmd title
#         cat's cmd
#             #+cat second cat
#             #: second title
#             second cmd
#             #+end_cat
#         #+end_cat
#         #: two
#         two's cmd
#         """)
#         import ipdb; ipdb.set_trace()
#         items = main(txt)

if __name__ == '__main__':
    unittest.main()
