#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys

_ITEM = '#+ '
_ITEM2 = '#:'  # we can delete this form
_CAT = '#+cat:'
_END_CAT = '#+end_cat'
_DOC = '#+doc:'
_IGNORE = '#+ignore'
"""Markers. No need of a marker for the shell command."""

_GUI = '#+gui'
_GUI_TOGGLE = _GUI+':toggle='

_IM = '#+im:'
"""Display local or remote images for the given element."""

def is_item_marker(line):
    """marker for a new item: not #+cat nor #gui markers.
    """
    return line.startswith("#+") or line.startswith("#:")

def is_item_title(line):
    return line.startswith(_ITEM) or line.startswith(_ITEM2)

def is_item_cat(line):
    return line.startswith(_CAT)

def is_item_end_cat(line):
    return line.startswith(_END_CAT)

def is_item_doc(line):
    return line.startswith(_DOC)

def is_item_image(line):
    return line.startswith(_IM)

def is_comment(line, ignore_marker=True):
    comment = ignore_marker
    if not ignore_marker:
        comment = not is_item_marker(line)
    return comment and line.startswith("#")

def is_block_comment(line):
    return is_comment(line) and not is_item_marker(line)

def next_lines_no_comment(cur, lines):
    """Get the next lines of this item which aren't shell comments: they
    are the command.
    """
    # cur: number of line coming just after the #:
    if cur >= len(lines):
        return None
    i = cur
    cmd = ""
    while i < len(lines) and is_comment(lines[i]):
        if is_item_title(lines[i]):
            return None
        i += 1
    while i < len(lines) and not is_comment(lines[i]):
        cmd += lines[i].strip("#")
        i += 1
    return cmd

def next_im(cur, lines):
    """Get the next image attribute within the scope of this item (do not
    cross the next no-commented line).
    """
    i = cur
    try:
        while is_comment(lines[i]):
            if is_item_image(lines[i]):
                return lines[i][len(_IM):].strip()
            i += 1
    except IndexError:
        return None


def doc_multilines(cur, lines):
    """Read the doc on multiple lines if needed.
    """
    # Start on the line following "#+doc:"
    i = cur
    doc = ""
    while i < len(lines) and\
          is_comment(lines[i]) and \
          not is_item_marker(lines[i]):
        doc += lines[i].strip("#")
        i += 1
    return doc

def next_doc(cur, lines):
    """return the line(s) of doc.
    """
    doc = None
    i = cur
    try:
        while i < len(lines) and is_comment(lines[i]):
            if is_item_doc(lines[i]):
                doc = lines[i][len(_DOC):].strip()
                doc += doc_multilines(i + 1, lines)
            i += 1
    except IndexError:
        return doc
    return doc

def next_toggle(cur, lines, options=None):
    """return the toggle option.
    """
    i = cur
    while is_comment(lines[i]):
        if lines[i].startswith(_GUI_TOGGLE):
            return lines[i][len(_GUI_TOGGLE):].strip()
        i += 1
    return options.get("gui_toggle")

def get_new_item_from_title(cur, lines, options=None):
    title = lines[cur][len(_ITEM):].strip()
    gui_toggle = options.get("_TOGGLE_COUR")
    cmd = next_lines_no_comment(cur + 1, lines)
    im = None
    doc = None
    toggle = None
    if cmd:
        # If no cmd, no need to look for the rest.
        im = next_im(cur, lines)
        doc = next_doc(cur, lines)
        toggle = next_toggle(cur, lines, options=options)

    return {
        "title": title,
        "cmd": cmd,
        "im": im,
        "doc": doc,
        "gui_toggle": toggle,
    }


def txt_to_lines(txt):
    lines = filter(lambda l: not l == "", txt.splitlines())
    # Remove an empty marker of title that would be void
    # (immediatly followed by anothe one).
    for i, elt in enumerate(lines):
        if is_item_title(elt) and i + 1 < len(lines)\
           and is_item_title(lines[i+1]):
            print "info: the element '{}' is void and will be ignored.".format(elt)
            del(lines[i])
    return lines


def cursor_next_block(cur, lines):
    """Return the line number of the beginning of the next item (title,
    category or end of a category).
    """
    # Pass all comments that are not markers, then all normal lines
    # (the command), and find the next marker for an item.
    i = cur
    while i < len(lines) and is_comment(lines[i], ignore_marker=False):
        i += 1
    while i < len(lines) and not is_comment(lines[i]):
        i += 1
    while i < len(lines):
        if is_item_title(lines[i]) or is_item_cat(lines[i]) or is_item_end_cat(lines[i]):
            return i
        i += 1
    raise IndexError

def go_after_cat(lines):
    import ipdb; ipdb.set_trace()

def parser(lines, cur=0):
    items = []
    options = {}
    try:
        while cur < len(lines):
            line = lines[cur]
            print "seeing line: ", line
            if is_item_title(line):
                item = get_new_item_from_title(cur, lines, options=options)
                items.append(item)

                cur = cursor_next_block(cur + 1, lines)

            elif is_item_cat(line):
                cat_title = line[len(_CAT):].strip()
                cat_doc = next_doc(cur+1, lines)
                # Call the parser recursively. We exit at the end of a
                # cat or at the end of file. We keep track of the line
                # we're on.
                cat_items, cur = parser(lines, cur + 1)
                items.append({
                    "cat": cat_title,
                    "doc": cat_doc,
                    "items": cat_items,
                    })


            elif is_item_end_cat(line):
                print "seeing end of cat"
                return (items, cur+1)

            else:
                print "line not recognized:", line
                cur += 1


    except IndexError:
        # End of the file.
        print "warning: IndexError"
        pass

    return (items, cur)

def main(txt):
    lines = txt_to_lines(txt)
    ITEMS, _ = parser(lines)
    return ITEMS

if __name__ == '__main__':
    if len(sys.argv) > 1:
        txt = open(sys.argv[1], 'rb')
        main(txt)

    else:
        print 'Usage : donnez un script à analyser en argument.'
