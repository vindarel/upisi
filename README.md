Upisi
=====

the Universal Post-Installation Scripts Interface.

Still in development but works fine on Debian, Ubuntu, Mint, Fedora,
Sabayon (and nearly OpenSuse).

(Lire en français: voir README-fr)

The goal of Upisi is to be able to write a shell script (typically, a
post-installation script for your favorite linux distro) and use Upisi
to:
- obtain a graphical Gtk+3 interface that allows to point and click to
  select the commands to be launched,
- use the graphical packages manager of your distro to install new
  software (this is much more user friendly for new users),
- clearly associate a command and its documentation, with images,
- clearly see which command failed.

We see two big use cases:
- we want to share a script of ours to someone not familiar with the
  command line,
- we took a post-install script from someone else but we don't want to
  bother read the code to choose what to install, we prefer to select
  a few packages with point and click.


Usage:

- write your script as usual, like

        apt-get install -y 0ad
        echo do something

- wrap up your code with upisi specific comments to control the gui:
    - #+title: a title
    - #+doc: a multi-line doc
    - #+sh: a title for that shell command,
    - #+im: a link to an image to be included in the documentation,
    - #+begin ... #+end to group many lines together
    - you can group items in categories with #+cat ... #+end_cat (you'll get a drop down arrow in the gui).

The above example can be written like so:

        #: A beautiful Age-of-Empires-like game
        #+doc: and it's free and libre !
        #+im: im/0ad.png
        apt-get install -y 0ad

To share your script: Upisi can open files and a git link (menu
Files-Open url… or Ctr-u).

## Full example ##

The following script…(still a valid shell script for the geeks)

    #!/bin/sh

    #+gui:toggle=False

    aptitude install npm

    #: Programmes pratiques
    sudo apt-get install -y terminator mplayer bash-completion git cclive htop tree ncdu python-pip soundconverter nautilus-open-terminal

    #+cat:Lecteurs Audio et Vidéo

    #: Clementine
    #+im:im/clementine.png
    #+doc: Lecteur très pratique (et multiplateforme)
    sudo apt-get install -y clementine
    etc…
    #+end_cat

gets automatically this fantastic gui:

![interface du script](im/universal-post-installation-scripts-insterface.png "interface générée")
![interface du script](https://gitlab.com/vindarel/upisi/raw/master/im/universal-post-installation-scripts-insterface.png "interface générée")

When you click on «validate», Upisi asks for confirmation and then
will use the graphical packages installer of your distro if needed and
it will show a popup window if some commands failed.

# Download and try it out #

A one-liner to get and run Upisi:

        git clone https://github.com/vindarel/upisi.git && cd upisi/ && python upotism.py

a few words about dev:
- postinstaller.py: the parser
- gui.glade: the gui (use glade to edit)
- upotism.py: main
- utils.py: some methods
- mint-postinstall.sh: a full example.

## Run the tests ##

With python's `unittest` module, you can run a single test method:

    python -m unittest test.test_parser.TestParser.test_categories

# Known bugs #

If you click on «validate» then go back to the main window, it won't
refresh.
