upisi
=====

the Universal Post-Installation Scripts Interface

En développement. Fonctionnel pour Debian, Ubuntu, Mint, Fedora,
Sabayon et presque pour OpenSuse.

Le but est d'écrire un script shell (typiquement, un script de
post-installation) et d'utiliser upisi pour:
- obtenir une interface graphique qui nous permet de cliquer pour
  sélectionner les commandes à effectuer,
- qui utilise le gestionnaire de paquets graphique de la distribution
  pour installer de nouveaux paquets,
- de voir clairement la documentation associée à une commande, avec
  des images, et de
- voir clairement quelles commandes ont réussi ou échoué.

On peut distinguer 2 cas d'usage:
- on veut partager un script avec quelqu'un qui n'est pas familier
  de la ligne de commande,
- on a un script qui installe beaucoup de paquets mais on veut en
  sélectionner seulement quelques uns, sans lire du code.


Usage :

- écrire un script bash de post-installation, comme d'habitude, de la forme :
  apt-get install -y foo bar
  echo do something

- enrober les commandes de tags spéciaux en commentaire pour agir sur
  l'interface graphique Gtk :
  - #+title: un titre
  - #+doc: de la documentation sur l'élément
  - #+im: lien vers une image à insérer dans la documentation.
  - #+sh: titre donné à cette commande shell (une ligne seule est
     interprétée par défaut comme une commande shell). Pour regrouper plusieurs commandes :
  - #+begin … #+end : toutes les commandes seront exécutées ensemble.

On peut organiser l'interface graphique par catégories :
   `#+cat: Une catégorie
    …
    #+end_cat`

Tout ce qui est dans la catégorie sera regroupé sous une flèche à
dérouler. On peut imbriquer autant de catégorie que l'on veut.

  - #+gui:toggle=False : indique à l'interface de ne pas sélectionner cet élément par défaut.

Exemple
=======

Le script shell suivant (qui reste un script shell valide pour les geeks):

    #!/bin/sh

    #+gui:toggle=False

    aptitude install npm

    #: Programmes pratiques
    sudo apt-get install -y terminator mplayer bash-completion git cclive htop tree ncdu python-pip soundconverter nautilus-open-terminal

    #+cat:Lecteurs Audio et Vidéo

    #: Clementine
    #+im:im/clementine.png
    #+doc: Lecteur très pratique (et multiplateforme)
    sudo apt-get install -y clementine
    etc…
    #+end_cat

Obtient l'interface graphique suivante (plus facile pour les non-geeks):

![interface du script](im/universal-post-installation-scripts-insterface.png "interface générée")
![interface du script](https://gitlab.com/vindarel/upisi/raw/master/im/universal-post-installation-scripts-insterface.png "interface générée")

Lorsqu'on clique sur `valider`, upisi récapitule les choix et demande
confirmation puis lance le gestionnaire de paquets graphique de la
distribution. Si des commandes sortent en erreur, une fenêtre nous les
affiche.

Télécharger et essayer
==========

Une commande pour télécharger et lancer Upisi : `git clone https://github.com/vindarel/upisi.git && cd upisi/ && python upotism.py`


Pour le dév :
- postinstaller.py : parser du script
- gui.glade : interface graphique Gtk+3 (utiliser glade)
- upotism.py : programme principal
- utils.py : méthodes annexes
- mint-postinstall.sh : exemple complet de script possible
